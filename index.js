const elementHandleClick = ['#product', '#services'];
const $ = (el) => document.querySelector(el);
elementHandleClick.forEach((el) => {
    // Không dùng arrow function chỗ này
    let state = false;
    $(el).onclick = function (elSelector) {
        state = !state;
        // Rotate mũi tên
        elSelector.preventDefault();
        this.querySelector('.fa-chevron-up').classList.toggle('rotate');

        // Show menu con
        let inline = 'none';
        if (state) {
            inline = 'block';
        }
        this.querySelector('ul').style.display = inline;
    }
})